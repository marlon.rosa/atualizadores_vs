#!/bin/bash

# USE VERSION ADMIN >= 1.44.0 IMPRESSORA GERTEC G250 AJUSTE TAMANHO IMPRESSAO

# Use sudo wget -O- https://gitlab.com/marlon.rosa/atualizadores_vs/-/blob/main/update_services_vs_food_V1.4.sh | bash
# Execute sudo bash update_services_vs_food.sh
# or chmod 777 update_services_vs_food.sh
# sudo ./update_services_vs_food.sh

# Versions
VsOsInterface="2.21.0"
VsPrint="2.20.0"
VsAutoPag="2.20.1"
VsFoodLauncher="1.3.0"

# Prepare
sudo rm /var/lib/dpkg/lock-frontend
sudo rm /var/lib/dpkg/lock
sudo ufw disable
sudo modprobe usbcore autosuspend=-1
sudo snap remove brave
# Error apport Ubuntu remove
sudo rm /var/crash/*
sudo apt remove apport apport-symptoms -y


# Stop all services
sudo service vs-os-interface stop
sudo service vs-print stop
sudo service vs-autopag-se stop

killall node
killall vs-os-interface
killall vs-autopag-se
killall vs-print

# Backup logs
mkdir -p /tmp/{vs-print,vs-autopag-se,vs-os-interface}

sudo cp /opt/videosoft/vs-print/.env /tmp/vs-print/

cp -R /opt/videosoft/vs-autopag-se/log/ /tmp/vs-autopag-se/
sudo cp /opt/videosoft/vs-autopag-se/.env /tmp/vs-autopag-se/
sudo cp /opt/videosoft/vs-autopag-se/CliSiTef.ini /tmp/vs-autopag-se/

# Remove current services
sudo rm -f /opt/videosoft/scripts/start*
sudo rm -f /opt/videosoft/*.tar.gz

sudo apt remove vs-print -y
sudo apt remove vs-autopag-se -y
sudo apt remove vs-food-launcher -y
sudo apt remove google-chrome-stable -y

sudo rm -R /opt/videosoft/vs-autopag-se
sudo rm -R /opt/videosoft/vs-print

# Download packages
cd /home/videosoft
#Download VS Autopag
wget -c https://cdn.vsd.app/softwares/vs-autopag-se/2.22.0/vs-autopag-se_2.22.0_amd64.deb
#Download VS Print
wget -c https://cdn.vsd.app/softwares/vs-print/2.20.0/vs-print_2.20.0_amd64.deb
#Download VS Food Launcher
wget -c https://cdn.vsd.app/softwares/vs-food-launcher/1.3.0/vs-food-launcher_1.3.0_amd64.deb
#Download VS OS Interface
wget -c https://cdn.vsd.app/softwares/vs-os-interface/2.21.0/vs-os-interface_2.21.0_amd64.deb

wget -c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

#VSPrint.env
sudo echo "# file, usb, network or pdf" >> /opt/videosoft/vs-print/.env
sudo echo "PRINT_METHOD=usb" >> /opt/videosoft/vs-print/.env
sudo echo " " >> /opt/videosoft/vs-print/.env
sudo echo "# caminho para gravar o arquivo pdf" >> /opt/videosoft/vs-print/.env
sudo echo "PATH_PDF=/opt/videosoft/" >> /opt/videosoft/vs-print/.env
sudo echo " " >> /opt/videosoft/vs-print/.env

#Stop Service
sudo service vs-autopag-se stop

# Start Service on Terminal
cd /opt/videosoft/vs-autopag-se/
sudo ./node/bin/node vs-autopag-se.js


# Install packages

sudo dpkg -i vs-print_2.20.0_amd64.deb
sudo dpkg -i vs-autopag-se_2.22.0_amd64.deb
sudo dpkg -i vs-os-interface_2.21.0_amd64.deb
sudo dpkg -i vs-food-launcher_1.3.0_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb


# Include Script VS Autopag-SE
sudo chmod 777 /opt/videosoft/vs-autopag-se/vs-autopag-se

# Restore logs
sudo cp /tmp/vs-print/.env /opt/videosoft/vs-print/
sudo mv .env /opt/videosoft/vs-print/
sudo cp -R /tmp/vs-autopag-se/log/ /opt/videosoft/vs-autopag-se/

# Restart services
sudo service vs-os-interface stop
sudo service vs-os-interface start
sudo service vs-print stop
sudo service vs-print start
sudo service vs-autopag-se stop
sudo service vs-autopag-se start

# Remove packages
rm *.deb

#google-chrome app.vsfood.com.br/status
echo "*****************Instalação Concluida*************************"
